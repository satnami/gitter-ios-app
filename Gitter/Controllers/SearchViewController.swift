import Foundation
import CoreData

class SearchViewController: UITableViewController, UISearchControllerDelegate, UISearchResultsUpdating, SuggestionsDataSourceDelegate, RoomSearchDataSourceDelegate, UserSearchDataSourceDelegate, HintViewDelegate {

    @IBOutlet var searchHeading: UIView!
    @IBOutlet var searchScope: UISegmentedControl!

    var showUserSearchOnAppear = false

    private let restService = RestService()
    private let suggestionsDataSource = SuggestionsDataSource()
    private let roomSearchDataSource = RoomSearchDataSource()
    private let userSearchDataSource = UserSearchDataSource()

    private var rootViewController: RootViewController?
    private var searchController: UISearchController?
    private var pullToRefreshControl: UIRefreshControl?
    private var noRoomsView: UIView?
    private var noUsersView: HintView?

    private var query = ""
    private var mode: Mode {
        get {
            if (searchController?.isActive ?? false) {
                switch SearchScope(rawValue: searchScope.selectedSegmentIndex)! {
                case .rooms:
                    return .roomSearch
                case .people:
                    return .userSearch
                }
            } else {
                return .suggestions
            }
        }
    }

    override func loadView() {
        super.loadView()

        let bundle = Bundle.main

        noRoomsView = bundle.loadNibNamed("NoResultsView", owner: self, options: nil)!.first as? UIView

        noUsersView = bundle.loadNibNamed("HintView", owner: self, options: nil)!.first as? HintView
        noUsersView?.button.setTitle("No Results. Invite?", for: UIControl.State())
        noUsersView?.delegate = self
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        rootViewController = (splitViewController as! RootViewController)

        tableView.register(UINib(nibName: "AvatarWithBadgeCell", bundle: nil), forCellReuseIdentifier: "AvatarWithBadgeCell")

        searchController = UISearchController(searchResultsController: nil)
        searchController!.searchResultsUpdater = self;
        searchController!.hidesNavigationBarDuringPresentation = false;
        searchController!.dimsBackgroundDuringPresentation = false
        searchController!.delegate = self

        let searchBar = searchController!.searchBar
        searchBar.searchBarStyle = .minimal;
        searchBar.sizeToFit()

        navigationItem.titleView = searchBar

        // this ensures that navigating to a search result hides the search bar as well
        definesPresentationContext = true

        // remove infinite separators
        tableView.tableFooterView = UIView()

        suggestionsDataSource.delegate = self
        roomSearchDataSource.delegate = self
        userSearchDataSource.delegate = self

        render()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        restService.getSuggestions { (error, didSaveNewData) in
            LogIfError("Failed to fetch suggestions", error: error)
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        if (showUserSearchOnAppear) {
            searchScope.selectedSegmentIndex = SearchScope.people.rawValue
            searchController?.searchBar.becomeFirstResponder()
            showUserSearchOnAppear = false
        }
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch mode {
        case .suggestions:
            let suggestion = suggestionsDataSource.getItemAtIndex(indexPath)
            rootViewController?.navigateTo(roomId: suggestion.id, sender: self)
        case .roomSearch:
            let room = roomSearchDataSource.getItemAtIndex(indexPath)
            rootViewController?.navigateTo(roomId: room.id, sender: self)
        case .userSearch:
            let user = userSearchDataSource.getItemAtIndex(indexPath)
            if let roomId = user.oneToOneRoom?.id {
                rootViewController?.navigateTo(roomId: roomId, sender: self)
            } else {
                rootViewController?.navigateTo(roomUri: user.username, sender: self, completionHandler: nil)
            }
        }
    }

    @IBAction func didPullToRefresh(_ sender: UIRefreshControl) {
        restService.getSuggestions { (error, didSaveNewData) in
            sender.endRefreshing()
            LogIfError("Failed to fetch suggestions", error: error)
        }
    }

    @IBAction func didChangeSearchScope(_ sender: UISegmentedControl) {
        render()
        search(query)
    }

    func didPresentSearchController(_ searchController: UISearchController) {
        render()
    }

    func didDismissSearchController(_ searchController: UISearchController) {
        render()
    }

    func updateSearchResults(for searchController: UISearchController) {
        search(searchController.searchBar.text!)
    }

    func didTapButton(_ hintView: HintView, button: UIButton) {
        let activityItems = [
            "Hey \(query), come join Gitter!",
            URL(string: "https://gitter.im?utm_source=ios&utm_medium=link&utm_campaign=ios-search-invite")!
        ] as [Any]
        let activityViewController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = button
        activityViewController.popoverPresentationController?.sourceRect = button.bounds
        self.present(activityViewController, animated: true, completion: nil)
    }

    private func search(_ query: String) {
        self.query = query

        switch SearchScope(rawValue: searchScope.selectedSegmentIndex)! {
        case .rooms:
            roomSearchDataSource.search(query)
        case .people:
            userSearchDataSource.search(query)
        }
    }


    func didChangeSuggestions(_ suggestionsDataSource: SuggestionsDataSource) {
        if (mode == .suggestions) {
            render()
        }
    }

    func roomSearchResultsDidChange(_ roomSearchDataSource: RoomSearchDataSource) {
        if (mode == .roomSearch) {
            render()
        }
    }

    func userSearchResultsDidChange(_ roomSearchDataSource: UserSearchDataSource) {
        if (mode == .userSearch) {
            render()
        }
    }

    private func render() {
        switch (mode) {
        case .suggestions:
            tableView.tableHeaderView = nil
            refreshControl = pullToRefreshControl
            tableView.dataSource = suggestionsDataSource
            tableView.backgroundView = nil
            tableView.reloadData()
        case .roomSearch:
            tableView.tableHeaderView = searchHeading
            refreshControl = nil
            tableView.dataSource = roomSearchDataSource
            if query.count > 1 && roomSearchDataSource.isEmpty() {
                tableView.backgroundView = noRoomsView
            } else {
                tableView.backgroundView = nil
            }
            tableView.reloadData()
        case .userSearch:
            tableView.tableHeaderView = searchHeading
            refreshControl = nil
            tableView.dataSource = userSearchDataSource
            if query.count > 1 && userSearchDataSource.isEmpty() {
                tableView.backgroundView = noUsersView
            } else {
                tableView.backgroundView = nil
            }
            tableView.reloadData()
        }
    }

    private enum Mode {
        case suggestions
        case roomSearch
        case userSearch
    }

    private enum SearchScope: Int {
        case rooms = 0
        case people = 1
    }
}
