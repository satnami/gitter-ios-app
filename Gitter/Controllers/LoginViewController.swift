import Foundation

class LoginViewController: UIViewController {

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier {
            switch identifier {
            case "login-github":
                let vc = segue.destination as! LoginOauthViewController
                vc.authProvider = "github"
            case "login-twitter":
                let vc = segue.destination as! LoginOauthViewController
                vc.authProvider = "twitter"
            default:
                // secret menu, do nothing
                return
            }
        }
    }
}
