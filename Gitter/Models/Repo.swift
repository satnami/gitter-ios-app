import Foundation

class Repo {
    let uri: String
    let isPrivate: Bool
    let description: String?
    var owner: String {
        get {
            return uri.components(separatedBy: "/")[0]
        }
    }
    var name: String {
        get {
            return uri.components(separatedBy: "/")[1]
        }
    }

    init(uri: String, isPrivate: Bool, description: String?) {
        self.uri = uri
        self.isPrivate = isPrivate
        self.description = description
    }
}
